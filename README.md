
# DRAWING WEB-APP

Sample code for a Drawing web app. Written in HTML/CSS/JavaScript + PHP + MySQL

# Drawing Funny App - Get X-Y testing

# App First Look
![Scheme](images/home.jpg)

After registed you able to login

# Drawing Screen.
![Scheme](images/DrawingScreen.jpg)

# Drawn Whatever.
![Scheme](images/Drawn.jpg)

# Quit Drawing.
![Scheme](images/quit.jpg)

# Result testing
![Scheme](images/results.jpg)





