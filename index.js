
// variables for image recognition.

var X_cor = new Array();
var Y_cor = new Array();
var stroke_array = [];
var stroke;
//var arr1 = new Array(X_cor,Y_cor);
var arr1 = [];

// setup variables for logging
var log = document.getElementById("log");

// set up the canvas
var context = document.getElementById("main").getContext("2d");

// setup default pen style
context.strokeStyle="#000";
context.lineJoin = "round";
context.lineWidth = 5;

// setup New startDrawing
document.getElementById("new").addEventListener("click", function() {
  context.clearRect(0, 0, mainCanvas.width, mainCanvas.height);
});

// set up watch variables
var startDrawing = false;
var mainCanvas = document.getElementById("main")


var dragging = false;

mainCanvas.addEventListener("mouseleave", function(){
  console.log("leaving the canvas!");
  startDrawing = false;
});


var flag = 0;

mainCanvas.addEventListener("mousedown", function(){
    flag = 0;
    startDrawing = true;

}, false);

mainCanvas.addEventListener("mousemove", function(){
    flag = 1;



    if (startDrawing == true) {
      console.log("dragging!");
      dragging = true;
      var x = event.pageX - mainCanvas.offsetLeft;
      var y = event.pageY - mainCanvas.offsetTop;
      context.lineTo(x, y);
      context.closePath();
      context.stroke();

      context.moveTo(x,y);


      X_cor.push(x);
      Y_cor.push(y);


    }
}, false);

mainCanvas.addEventListener("mouseup", function(){

    startDrawing = false;

    if(flag === 0){
        console.log("click");

        // draw a rectangle
        var brushSize = context.lineWidth;

        var x = event.pageX - mainCanvas.offsetLeft;
        var y = event.pageY - mainCanvas.offsetTop;


        context.fillRect(x,y,brushSize,brushSize);
    }
    else if(flag === 1){
        console.log("drag");
        context.beginPath();


        // code to build  JSON

        arr1.push(X_cor,Y_cor);

        var myJSON = JSON.stringify(arr1);
        $("#temporary").replaceWith(myJSON);


        // code to build the stroke json
        stroke_array.push(arr1);
        stroke = JSON.stringify(stroke_array);


         var temp_string ="<h4 id='temporary'> hello </h4>";
         $("#temporary").replaceWith(temp_string);
        // code to build the stroke json

        console.log(x,y);
        X_cor = [];
        Y_cor = [];
        arr1  = [];
    }
});


//END DRAWING CODE -------------------------------------------------

// start drawing button
$("#btn").click(function(){
  console.log("start button");
  $("#div1").hide();
  $("#div2").hide();
  $("#div3").hide();
  $("#div0").show();
  $("#div4").hide();
  countdown();
});//end

// got button
$("#got").click(function(){
  console.log("got button");

  $(final_string)
  $("#div1").hide();
  $("#div2").hide();
  $("#div3").hide();
  $("#div0").show();
  $("#div4").hide();
  countdown();
});//end

//play-again button
$("#play-again").click(function(){
  console.log("play-again button");
window.location.href = "index.html";
});//end
//play-again button
$("#close").click(function(){
  console.log("close button");
  $("#div1").hide();
  $("#div2").hide();
  $("#div3").hide();
  $("#div4").show();
});//end

//No button
$("#no").click(function(){
  console.log("no button");
  $("#div1").hide();
  $("#div2").hide();
  $("#div3").hide();
  $("#div4").hide();
});//end
//Yes button
$("#yes").click(function(){
  console.log("yes button");
  window.location.href = "index.html";
});//end

// clear what u had drawn before
document.getElementById("got").addEventListener("click", function() {
  context.clearRect(0, 0, mainCanvas.width, mainCanvas.height);
});
var turn = 0;
//count down
function countdown(){
  var timeleft = 20;
    var downloadTimer = setInterval(function(){
    timeleft--;
    document.getElementById("clock-time").textContent = timeleft;
    if(timeleft <= 0)
        clearInterval(downloadTimer);
        if (timeleft == 0){
          //when time end do something here.
          console.log("end time");
          //turn
          if (turn == 6){
            console.log("ran out of turn");
          }
          else {

            var final_string = "<input id='debug' type='text' name='data' value=" +stroke+ ">";
            //var final_string = "<h7 id='debug'>" +stroke+ "</h7>";
            $("#debug").replaceWith(final_string);

            $("#div2").show();
            $("#div0").hide();
            $("#div1").hide();
            $("#div3").hide();
            $("#div4").hide();
            turn = turn + 1;
            console.log(turn);
            document.getElementById("turn").textContent = turn;
          }if(turn == 6){
            //turn out, then show the pictures had drawn from shit database here.
            console.log(" You went shit out of turn");
            $("#div0").hide();
            $("#div1").hide();
            $("#div2").hide();
            $("#div3").show();
            $("#div4").hide();
          }//end turn
        }//end if timeleft ==0;
    },1000);
}//end countdown
